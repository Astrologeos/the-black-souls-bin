# Black Souls

![](https://img.dlsite.jp/modpub/images2/work/doujin/RJ252000/RJ251629_img_main.webp)

![](https://img.dlsite.jp/modpub/images2/work/doujin/RJ238000/RJ237469_img_main.webp)

![](https://i.imgur.com/C6xJHph.jpg)

## Downloads

### Games

Black Souls 1 (DLCs included)

https://bantcraft.serveminecraft.net/RJ251629.zip

Red Riding Woods

https://ipfs.infura.io/ipfs/QmWTDZykKkDSWJhwLA5Dg7i36Di4nWvc9Q6JNRWvqCCYtu/RJ259845%20-%20Red%20Riding%20Woods.7z

Black Souls 2 (DLCs included)

https://ipfs.io/ipns/k2k4r8l7mxpi57sotykoy5f5ucakg0dr0ib0avmyjhwmofkvpfhfd510/DL/RJ237469%20-%20Black%20Souls%20II.7z

### Obsolete

Black Souls 2 (Pre DLC 3)

https://workupload.com/file/6hkmQAmg6pE

### Utilities

English RTP for RPG Maker VX Ace (to avoid potential crashes): https://dl.degica.com/rpgmakerweb/run-time-packages/RPGVXAce_RTP.zip

Fullscreen++ v2.2 Patches: [See the Patches folder](./Patches)

Alicespeak Converter: https://tiphereth.herokuapp.com/bs/translator/

## Walkthroughs, Guides, Location Lists

**SPOILERS BELOW, READ AT YOUR OWN RISK**

Black Souls 1 Walkthrough: https://pastebin.com/r6m5iTs0

Black Souls 1 Scenes Unlocking Guide: https://pastebin.com/DGufJA6C

Black Souls 2 Walkthrough (Pre DLC 3): https://docs.google.com/document/d/1aQAFJKY6tBF-HDrtRlvV1f6sfTCmMuQFfvQcmyjvSoc/edit

Japanese Wiki: https://w.atwiki.jp/iniminimanimo/

BS2 Korean Wiki: https://namu.wiki/w/BLACK%20SOULS%202

### BS1 Fairy Tale Locations

**[Hansel and Gretel]**: Kill them. They're in The Witch's house.

**[Three Little Pigs]**: Behemoth Ranch, after defeating the Hyena of Hunger.

**[King's New Clothes]**: Kill the Iron King in the House of Truth at the Rotten Burg.

**[Knight in Armor]**: Lost Empire Castle's Entrance.

**[Dog of Elixir]**: Elixir Town's Cathedral.

**[Singing Bone]**: Skeleton Maze, towards the south from the bonfire.

**[Pied Piper of Elixir]**: Sewers at north of Dorothy the Witch's house.

**[Musicians of Bremen]**: Behemoth Ranch inside a cabin.

**[Ugly Duckling]**: Fort Ivern's Jail, the place where the game starts.

**[Jack and Beanstalk]**: Forest of Abandoned, near a pond and a fallen tree that serves as a shortcut.

**[Peter Pan]**: Gothel's Tower after pulling the lever that opens the path to Rapunzel.

**[Adventures of Sinbad]**: Hotel Poseidon's deck/2nd floor after dealing with Little Mermaid.

**[Alladin and Wonderful Lamp]**: Helsa Desert, right below a pyramid with some chests.

**[King with Donkey Ears]**: Snow White Castle's 2nd floor. He's sitting on the throne.

**[Iron Hans]**: Skeleton Maze, on top of the hill to the north after defeating Singing Bone.

**[Beauty and Beast]**: Forgive her after defeating her, and then re-check the ballroom later.

**[Tortoise and Hare]**: Meet the Tortoise and the Hare at various points and then go to The Secret Garden which is all west from Elixir Town.

  * Tortoise is in Forest of Abandoned and later in Jacob's Disused Mine, Hare is in Fort Ivern's Jail.

**[Pinocchio]**: Kill Pinocchio in Hotel Poseidon.

**[Rapunzel]**: Kill Rapunzel in Gothel's Tower.

**[Princess Frog]**: Kill Princess Frog in Tainted Bog.

**[Little Mermaid]**: Kill Little Mermaid in her house at Atlantida.

**[Snow White]**: Kill Snow White in the basement of the Snow White Castle at Garden of Snow.

**[Cinderella]**: Kill Cinderela at Lost Empire's Castle.

**[North Wind and Sun]**: Kill them. They're Cinderella's bodyguards.

#### Purist:

**[Alice in Wonderland]**: Kill Alice-02 in the library of Wonderland.

**[Wizard of Ox]**: Take Dorothy to Hotel Poseidon.

**[Jeanne d'Arc]**: Take Jeanne to Hotel Poseidon.

**[Golden Goose]**: Take Goose to Hotel Poseidon.

**[Saint of Alexandria]**: Take Catherine to Hotel Poseidon.

**[Queen Elisabeth]**: Take Elisabeth to Hotel Poseidon.

**[Little Red Riding Hood]**: Take Red Hood to Hotel Poseidon.

**[Little Match Girl]**: Take Elma to Hotel Poseidon.

#### Sinner:

**[Alice in Wonderland]**: Kill Alice-02 in the library of Wonderland.

**[Wizard of Ox]**: Rape and Kill Dorothy, or kill her in her cabin at Forest of Abandoned.

**[Jeanne d'Arc]**: Kill Jeanne after reviving her, or kill her as a demonbeast in Lost Empire Castle.

**[Golden Goose]**: Don't help her at Behemoth Ranch or Sea of Trash and then go to Lost Empire Castle, or rape and kill her.

**[Saint of Alexandria]**: Rape and Kill Catherine.

**[Queen Elisabeth]**: Rape and Kill Elisabeth. Alternatively, tell Red Hood to talk to her and then kill her in the Forest of Abandoned.

**[Little Red Riding Hood]**: Kill Red Hood in her house at west from Holy Forest.

**[Little Match Girl]**: Rape and Kill Elma, or kill her as a demonbeast outside of her shop.

### BS1 Companions List

**Leaf**: Walk north from the Holy Forest hub.

**Goose**: Free her in Behemoth Ranch, give her 1000 souls after dealing with 2 evil princesses, meet her in the tent at Sea of Trash and convince her to stay with you.

**Dorothy**: Speak to her in her cabin at Forest of Abandoned, become her apprentice, kill her 3 former apprentices (Warehouse, Forest of Abandoned, Behemoth Ranch) and invite her to Holy Forest.

**Miranda**: Enter the Tainted Bog with 5 Sins or more, defeat her and tell her to become your companion.

**Elma**: Kill the Pied Piper in the Sewers, visit her shop before and after killing the Iron King and then save her, before dealing with 4 of the 5 evil princesses.

**Red Hood**: After killing 4 of the demonic princessses or joining covenants with them, ride the boat at Holy Forest and go west.

**Elisabeth**: After killing or entering a covenant with 3 demon princesses go to the north from Holy Forest hub and help her find her ring. It's dropped by a fairy in the Forest of Abandoned close to a chest.

**Victoria**: Speak to her in the House of Truth after defeating the Iron King.

**Jeanne**: Speak to Shaman Keto inside Skeleton's Maze, pick up Jeanne's ashes where she died at Fort Ivern, defeat the kraken at the Sea of Trash, and speak to Keto again before killing or entering a covenant with 3 demon princesses.

**Catherine**: Defeat Patrasche in Elixir Town's Church, speak to her at Holy Forest, save her from her demonbeast-turned companions, revive Jeanne and then speak to her in Elixir Town's Church.

### BS2 Farming Strategies

- Crash Chamber at 0 Sen (a fine place to get your first hundreds of levels).

- Boss rushes at the Chaos Dungeon.

- Rabbit Kingdom at Rabbit Hole (needs 1 Rabbit Key per visit and the enemies don't respawn).

- Chaos Dungeon (sell the loot, defeat the enemies, catch the grey fairies which gives you your level * 1000 in souls).

- Miranda at Beach of Grief with Silver Ring of Avarice and Difficulty 9, 1950000 souls per battle, 51 battles to reach the cap.

### BS2 Considerations for a perfect NG+ cycle

**Goals**: Every Fairy Tale, every sex scene, F Ending and DLC3.

- You can aim for this after doing 3 regular runs to unlock the 2 scenes of the 3 Alices (Sister, Mommy and Daughter), and along the way, every other scene possible.

- You'll always want to trigger the F/G Endings Path by killing the Headhunting Beast and then entering Lutwidge Town at 0 Sen to meet Red Hood, so you can get Elisabeth Bathory's book later.

  - But also so you can do the G Ending and then jump to DLC3 soon afterward after you're thrown into a new cycle.

- Sherlock Holmes (Prince Leopold) will kill Mary on Progress 4, so by Progress 3 you MUST have slained him or be in front of his fog gate.

- For that reason, going after Jubjub asap is the best plan. You kill 2 birds with 1 stone.

  - Route: Mental Ward (Get Edith's Ring) -> Park of Fog -> Upper Lutwidge -> Ox Ward University -> Endless Tea Party -> Sick Clock Toweer.

  - If you want to be extra safe, you can do Park of Fog and Upper Lutwidge before the Mental Ward. That way, you'd have dealt with Jack by Progress 3.

- If you start Vernai's quest to slay the nightmares and don't see it through, or if you avoid him in Rabbit Hole, his fairy tale appeears in Carroll River after killing Jabberwock.

- You can lock yourself out of Dr. Blackwell's questline if you reach Progress 10 and you never talked to her in Ripon Cathedral after meeting her in Lutwidge Town.

- This should be obvious, but whenever you reach a new bonfire you should speak with Red, and if you unlock all 14 Pre Progress 1 locations, you should visit those before going after Edith's Ring.

  - She has special scenes too; 1 in Upper Lutwidge and 1 in Hotel Poseidon.

### BS2 Fairy Tales List

**[Wolf and XXX Young Goats]**: Liddell Cemetery.

**[The Ugly Duckling]**: Liddell Cemetery-Lutwidge Town Bridge.

**[The Jumper]**: Kill Insolent Grasshopper in Lutwidge Town's bar, Skipjack in Lutwidge Town Sewers and Unaccounted Flea in Upper Lutwidge.

**[Rascal]**: Library Dream, Fairy Tale Archive.

**[The Fox and The Grapes]**: Meet Griphy in Tenniel Bridge (Lutwidge Town), Ripon Cathedral and then Garden of Heart's eastern entrance.

**[Knight in Armor]**: Spore Forest. Summon them in the battle against Old Knight William.

**[Hansel and Gretel]**: Infinite Food. Summon them in the battle against Pregnant Cake.

**[The Crab and The Monkey]**: Billingsgate Market. Fight the monkey twice and then kill the crab.

**[The King with Donkey Ears]**: Red Queen's Throne. Summon him in the battle against her knights.

**[Black Rabbit of Inaba]**: Kill Vernai (Rabbit Hole, Billingsgate Market, Carroll River) or complete his questline.

**[Adventures of Sinbad]**: Billingsgate Market. Summon him for the Bellcaller Bellman boss battle.

**[Bluebeard]**: Pond of Bloody Tears, Torture Chambers.

**[Elizabeth Bathory]**: Pond of Bloody Tears, 0 SEN, Ending F/G path.

**[Musicians of Bremen]**: Slaughterhouse. Summon them in the fight against Beastclad Butcher.

**[Iron Hans]**: Mental Ward. Summon him in the fight against Great Grey Eagle Edith.

**[Wizard of Oz]**: Mental Ward, 100 SEN, after meeting a friendly face.

**[Boy Who Cried Wolf]**: Upper Lutwidge.

**[Robin Hood]**: Fuming Forest. Summon him in the fight against Bandersnatch.

**[Daddy-Long-Legs]**: Queensland. Summon him in the fight against Sibimet and Tabikat.

**[Mt. Kachi Kachi]**: Fuming Forest.

**[Pooh Bear]**: Queensland.

**[The Gigantic Turnip]**: Nameless Forest.

**[The Dog and The Shadow]**: Carroll River.

**[Dog of Flanders]**: Carroll River. Summon Patrasche in the fight against Beast of Conceit.

**[Goose That Laid Golden Eggs]**: Feed 900k souls to Humpty Dumpty in Carroll River, and kill the spawn.

**[The Little Match Girl]**: Lutwidge Town, 0 SEN.

**[Peter Pan]**: Sick Clock Tower. Summon him in the battle against Jubjub.

**[The Little Prince]**: Sick Clock Tower. Summon him in the battle against Beast of the Bright Star.

**[Saint of Alexandria]**: Oxward University. Pray to St. Catherine's statue and then summon her in the fight against Dean.

**[Jeanne d'Arc]**: Fuming Forest, behind the lava waterfall (going from the right).

**[Pinocchio]**: Deep Sea. Kill Whale Beast and then summon him in the battle against Deep Sea Knight.

### BS2 Chaos Dungeon Exclusive Content

**Weapons/1st Hand**: Lost Sword, Gla'akid, Mary's Magical Wand.

**Shields/2nd Hand**: Cannon, Bean Tree Shield, Mercury Shield, Miracle Maiden's Garb, Chaplain's Robe, Armor of Iron Valor.

**Skills**: Chaos Blast, Royal Tea, Quick Dance, Catherine's Wheel, Holy Banner, Black Wave, Black Slash, Awakening.

### BS2 Jack-in-the-box Rewards

*Contents: Item, Location, Reward.*

Soul of the Head-Hunting Beast, Rabbit Hole, Vorpal Blade

Soul of Distraction, Liddell Cemetery-Lutwidge Town Bridge, Porcupine Shield

Soul of the Pregnant Cake, Infinite Food, Spell Stoneplate Ring

Soul of the Bellcaller, Billingsgate, Ghostfire Skill Book

Soul of the Butcher, Slaughterhouse, Butcher's Chainsaw

Soul of the Gray Eagle, Mental Ward, Ring of Steel Protection

Soul of Conceit, Dummy, Rampage Skill Book

Soul of Jack, Upper Lutwidge, Manslayer's Shotgun

Soul of Dean, Ox Ward University, Omnibless Skill Book

Soul of the Bright Star, Sick Clock Tower, Ring of Heaven

Soul of the Old Knight, Spore Forest, Stone Flesh Skill Book

Soul of the Giant's House, Mushroom Village, Black Rabbit Ring

Soul of the Knight of Hearts, Queen's Room in Red Castle Frissel, Knight of Hearts' Ring

Soul of the Knight of Spades, Queen's Room in Red Castle Frissel, Knight of Spades' Ring

Soul of the Knight of Clubs, Queen's Room in Red Castle Frissel, Knight of Clubs' Ring

Soul of the Slave Emperor, Queensland, King's Order Skill Book

Soul of the Slave Empress, Queensland, Heavy Soul Discharge Skill Book

Soul of Queen of Torture Tools, Pond of Bloody Tears, Omnicurse Skill Book

Soul of the Bandersnatch, Fuming Forest, Violent Sword Bandersnatch

Soul of the Jubjub, Sick Clock Tower, Mad Bow Jubjub

Soul of the Jabberwock, Carroll River, Rotten Scythe Jabberwock

Soul of the God's Odd Fish, Ship Graveyard, God Fish's Ring

Soul of the Deep Sea Knight, Deep Sea, Deep Sea Knight's Anchor

Soul of the Dragon Hunter, Rabbit Hole, Heroic Vorpal Sword

Soul of the Wet Nurses, Crimea Nursery Graveyard, Guardian Angel

Soul of Florence, Crima Nursery Graveyard, Holy Gunblade

Soul of the Winterbell's Wind, Windless Valley, Windless Wrap

Soul of the White Unicorn, Dueling Grounds, Unis' Rapier

Soul of the White Lion, Dueling Grounds, Leiden's Twin Axes

### BS2 Rape Dungeon 

From left to right:

**1st Floor**: Beggar Corpse, Mary, Griphy, Margaret, Tweedledum and Tweedledee, Bill, Dodo, Shisha, Golden Goose, Lorina, Mockie, Ox Ward Student, March Hare, Dormouse, Hatter Hata, Dr. Blackwell

**2nd Floor**: Wolris, Cheeky Oyster, Kuti

**3rd Floor**: Bandersnatch, Jubjub, Jabberwock

If you complete the entire 1st floor after having put Dormouse back in her cell, a super funny event will happen once you re-enter the map.

### BS2 Ending F/G, Red Hood locations

You need to interact with Red in at least 10 of these places as a pre-requisite to unlock Endings F/G.

- Rabbit Hole

- Liddell Cemetery

- Ripon Cathedral

- Mushroom Village

- Pond of Bloody Tears (Boss Battle)

- Fuming Forest

- Billingsgate Market

- Slaughterhouse

- Upper Lutwidge

- Ox Ward Academy

- Carroll River

- Garden of Heart

- Red Castle Frisel

- Duchess' Mansion

- Beach of Grief

- Hotel Poseidon

### BS2 Griphy Questline Locations

1) Tenniel Bridge (Lutwidge Town)

2) Ripon Cathedral

3) Garden of Heart, eastern entrance

4) Red Queen's Castle after defeating her knights if you didn't kill or rape her

5) Beach of Grief

6) Queensland

7) Red Queen's Castle, queen's room

Tip: If you don't want to lose Lorina in your current cycle, you should stop at 6, otherwise keep going for a threesome at Mockie's Restaurant.

### BS2 About Progress Points

You get a progress point if you:

- Defeat a fog gate boss (Rascal and The Duchess don't count)

- Reach Upper Lutwidge

- Pull the lever at Ripon Cathedral

- Interact with Alice in Spore Forest

If you get 2 progress points or more, you will automatically be locked out of the true ending path that leads to Endings F and G.

If you hit 4 without killing a certain boss, a certain character will be killed.

If you hit 10 without killing or raping a certain different boss, funny stuff happens across the game's world after interacting with a bonfire.

Progress points can be checked at the Dream Library; each lit candle represents one progres point.

### BS2 Liddell House VN Best Ending Walkthrough

1) Do whatever you want until the night of Day 2.

2) Leave to the 2F corridor right away and inspect the third daughter's room afterward, that's Edith's.

3) Go to the 1st Floor Corridor, and from there to the Living Room.

4) Inspect 4 times to get the medicine.

5) Go back to the 2nd floor and inspect the third daughter's room to give Edith her medicine.

6) On the next day, go to the Living Room and speak with the Eldest Daughter, Lorina.

7) As soon as the night comes, go to her room.
