1. Open the Data folder in the game's folder.
2. Find Scripts.rvdata2 in that folder. Rename it to something else.
3. Take the Scripts.rvdata2 from this archive and paste into the Data folder.

F8 for fullscreen, F9 to change aspect ratio. 
This was made for the latest English build currently available of Black Souls 2, don't be surprised if it doesn't work on a future version of the game.