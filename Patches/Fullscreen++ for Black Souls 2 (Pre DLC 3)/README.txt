1. Use the Decrypter on the game's .rgss3a file to extract its contents.
2. Open the Data folder in the game's folder.
3. Find Scripts.rvdata2 in that folder. Rename it to something else.
4. Take the Scripts.rvdata2 from this archive and paste into the Data folder.

F8 for fullscreen, F9 to change aspect ratio. 
This was made for the latest English build of Black Souls 2 before the DLC 3 was released, don't be surprised if it doesn't work on a future version of the game.